﻿using InterfaceLib;
using System;

namespace PluginLib1
{
    public class PluginClass1 : PluginInterface
    {
        public override string Name => "第一个插件";

        public override Version Version => Version.Parse("1.0.0");

        public override void Load()
        {
            Console.WriteLine("Load" + this.GetType().FullName);
        }

        public override string Method1()
        {
            return "这是从第一个插件里面来的数据";
        }

        public override string Method2(string from)
        {
            return "这是从第一个插件里面来的数据:" + from;
        }
    }
}
